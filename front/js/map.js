ymaps.ready(init)
async function init(){
    let pointers = await api.getPointers()
    console.log("🚀 ~ file: map.js ~ line 4 ~ init ~ pointers", pointers)
    let myMap = new ymaps.Map("map", {
        center: [54.97,82.90],
        zoom: 10
    })
    pointers.forEach(pointer => {
        let myPlacemark = new ymaps.Placemark([pointer.coordinates[0], pointer.coordinates[1]],  {
            balloonContent: `<div class='pointer'><div class='tittle'>Пункт по адресу ${pointer.address}</div>
                <button type="button"><a href='/catalog/${pointer._id}'>Выбрать велосипед</a></button></div>`,
            iconContent: pointer.address}
        , {
            preset: "islands#yellowStretchyIcon",
            balloonCloseButton: true,
        })
        myMap.geoObjects.add(myPlacemark)
    });
}