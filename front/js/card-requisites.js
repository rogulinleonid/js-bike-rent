cardNumber.addEventListener('blur', () => cardNumberValidate(cardNumber.value))
term.addEventListener('input', () => cardDateFormate(term.value))
term.addEventListener('blur', () => cardDateValidation(term.value))
cvv.addEventListener('input', () => cvvValidate(cvv.value))
cvv.addEventListener('blur', () => { if (cvv.value.split('').length === 3) hideError(document.querySelector('.cvv_wrapper'))})
save.addEventListener('click', () => commonValidate())

function cardNumberValidate(cardNumberValue) {
    cardNumberValue = cardNumberValue.toString().trim().split(' ').join('')
    let err = Number.isNaN(+cardNumberValue) ? 'Поле должно содержать только цифры' : false
    err = !err && cardNumberValue.split('').length !== 16 ? 'Поле должно состоять из 16 цифр' : err
    if (!err) { 
        cardNumberFormate(cardNumberValue)
        hideError(document.querySelector('.cardNumber_wrapper'))
    }
    else renderError(err, document.querySelector('.cardNumber_wrapper'))
}

function cardDateValidation(cardDateValue) {
    let date = cardDateValue.split('/')
    let el = document.querySelector('.term_wrapper')
    if (date[0] > 12) {
        renderError('Введенный номер месяца не существует', el)
        return
    }
    if (date[1] > currentYear - 2000) {
        renderError('Некорректно введен год выдачи карты', el)
        return
    }
    if (date[2] < currentYear) {
        renderError('Некорректно введен срок окончания действия карты', el)
        return
    }
    if (date[2] - date[1] - 2000 > 5) {
        renderError('Срок действия карты не может быть больше 5 лет', el)
        return
    }
    else hideError(el)
}

function commonValidate() {
    const textError = 'Поле не может быть пустым'
    if (cardNumber.value === '') renderError(textError, document.querySelector('.cardNumber_wrapper'))
    if (term.value === '' || term.value.split('').length < 10) renderError(textError, document.querySelector('.term_wrapper'))
    if (cvv.value === '') renderError(textError, document.querySelector('.cvv_wrapper'))
}

function cardDateFormate(cardDateValue) {
    let valueArr = cardDateValue.split('/').join('').split('')
    if (!Number.isInteger(+valueArr[valueArr.length - 1])) valueArr.pop()
    if (valueArr.length > 2) valueArr.splice(2, 0, '/')
    if (valueArr.length > 5) valueArr.splice(5, 0, '/')
    if (valueArr.length > 10) valueArr.pop()
    term.value = valueArr.join('')
}

function cvvValidate(cardCvvValue) {
    let cvvArr = cardCvvValue.split('')
    if (!Number.isInteger(+cvvArr[cvvArr.length - 1])) cvvArr.pop()
    if (cvvArr.length > 3) cvvArr.pop()
    cvv.value = cvvArr.join('')
}

function renderError(error, elem) {
    if (elem.lastChild.className === 'errorElem') {
        elem.lastChild.remove()
    }
    elem.style.position = 'relative'
    let errorElem = document.createElement('div')
    errorElem.className = 'errorElem'
    errorElem.style.position = 'absolute'
    errorElem.style.left = `${elem.offsetWidth}px`
    errorElem.style.top = '-8px'
    let errorElemInner = document.createElement('div')
    errorElemInner.innerText = error
    errorElem.appendChild(errorElemInner)
    elem.appendChild(errorElem)
}
    

function cardNumberFormate(cardNumberValue) {
    cardNumberValue = cardNumberValue.toString().split('')
    let str = []
    for (let i = 0; i < cardNumberValue.length; i++) {
        if (i % 4 === 0) str.push(' ')
        str.push(cardNumberValue[i].toString())
    }
    cardNumber.value = str.join('')
}

function hideError(elem) {
    if (elem.lastChild.className === 'errorElem') {
        elem.lastChild.remove()
    }
}

const currentYear = new Date().getFullYear()
const currentMonth = new Date().getMonth()