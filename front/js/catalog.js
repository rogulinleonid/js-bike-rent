'use strict';

function init() {
  let countPage = 1
  loadCatalog(countPage++)
  document.querySelector('#loadMore button').addEventListener('click', () => loadCatalog(countPage++))
}

async function loadCatalog(page = 1) {
  let bikes = await api.getBikes(document.URL.split('/')[4], page)
  showButtonLoadMore(bikes.hasMore)
  appendCatalog(bikes.bikesList)
  showEllipse()
}

function appendCatalog(items) {
  let countBikes = 0
  let leftColumn = document.querySelector('.left-bikeList')
  
  let rightColumn = document.querySelector('.right-bikeList')
  for (let item of items) {
      let bike = createBike(item)
      if (countBikes % 2 === 0) {
        leftColumn.appendChild(bike)
      } else {
        rightColumn.appendChild(bike)
      }
      countBikes++
  }
}

function createBike(item) { 
  let bike = document.createElement('div')
  bike.innerHTML = 
    `<div class="bikeImg"><div><img src="../images/${item.img}" alt="" height="148" width="250"></div></div>
    <div class="captionBike">${item.name}</div>
    <div class="priceBike">Стоимость за час - 240 ₽</div>
    <div class="buttonRent"><button type="button">Арендовать</button></div>`
  return bike
}

function showButtonLoadMore(hasMore) {
  let button = document.querySelector('#loadMore')
  if (hasMore) {
    button.style.display = 'block'
  }
  else button.style.display = 'none'
}

function showEllipse() {
  let id = document.URL.split('/')[4] === undefined ? 'allBikes' : document.URL.split('/')[4]
  let el = document.getElementById(id)
  el.style.position = 'relative'

  let ellipse = document.createElement('div')
  ellipse.style.width = '10px'
  ellipse.style.height = '10px'
  ellipse.style.background = 'no-repeat center, url(/images/img/Ellipse.jpg)'
  ellipse.style.position = 'absolute'
  ellipse.style.left = '-20px'
  ellipse.style.top = '8px'

  el.appendChild(ellipse)
}

document.addEventListener('DOMContentLoaded', init)